//
// Created by Галина Кокшарова on 08.03.2023.
//

#include <iostream>
#include "Cat.h"

void Cat::Voice() {
    Animal::Voice();
    std::cout << "meow!\n";
}

Cat::Cat() {
    aName = "Cat";
}
