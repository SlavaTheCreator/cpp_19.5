#include <iostream>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Cow.h"

int main() {
    Animal* animals[] = {new Dog, new Cat, new Cow};

    for(Animal* a: animals){
        a->Voice();
    }
    return 2;
}