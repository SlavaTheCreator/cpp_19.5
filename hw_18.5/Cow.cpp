//
// Created by Галина Кокшарова on 08.03.2023.
//

#include <iostream>
#include "Cow.h"

void Cow::Voice() {
    Animal::Voice();
    std::cout << "muuu!\n";
}

Cow::Cow() {
    aName = "Cow";
}
