//
// Created by Галина Кокшарова on 08.03.2023.
//

#include <iostream>
#include "Dog.h"

void Dog::Voice() {
    Animal::Voice();
    std::cout << "Woof!\n";
}

Dog::Dog() {
    aName = "Dog";
}
