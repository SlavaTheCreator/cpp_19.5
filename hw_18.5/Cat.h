//
// Created by Галина Кокшарова on 08.03.2023.
//

#ifndef HW_18_5_CAT_H
#define HW_18_5_CAT_H


#include "Animal.h"

class Cat: public Animal{
public:
    Cat();
    void Voice() override;
};


#endif //HW_18_5_CAT_H
