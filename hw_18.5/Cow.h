//
// Created by Галина Кокшарова on 08.03.2023.
//

#ifndef HW_18_5_COW_H
#define HW_18_5_COW_H


#include "Animal.h"

class Cow: public Animal{
public:
    Cow();
    void Voice() override;
};


#endif //HW_18_5_COW_H
