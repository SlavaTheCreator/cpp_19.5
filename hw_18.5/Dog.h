//
// Created by Галина Кокшарова on 08.03.2023.
//

#ifndef HW_18_5_DOG_H
#define HW_18_5_DOG_H


#include "Animal.h"

class Dog: public Animal{
public:
    Dog();
    void Voice() override;
};


#endif //HW_18_5_DOG_H
